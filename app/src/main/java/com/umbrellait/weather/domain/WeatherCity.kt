package com.umbrellait.weather.domain

import com.umbrellait.weather.domain.items.DisplayableItem

data class WeatherCity(
    val coord: Coord,
    val weather: Weather,
    val main: Main,
    val wind: Wind,
    val clouds: Clouds,
    val rain: Rain,
    val snow: Snow,
    val dt: Long,
    val son: Son,
    val timezone: Int,
    val id: Int,
    val name: String,
    val visibility: Int
): DisplayableItem

data class Coord(
    val lat: String,
    val lon: String
)

data class Weather(
    val id: Int,
    val main: String,
    val description: String,
    val icon: String
)

data class Main(
    val feelsLike: Double,
    val humidity: Int,
    val pressure: Int,
    val temp: Int
)

data class Wind(
    val deg: Int,
    val speed: Float
)

data class Clouds(
    val all: Int
)

data class Rain(
    val lastHour: Double
)

data class Snow(
    val lastHour: Double
)

data class Son(
    val sunrise: Long,
    val sunset: Long
)