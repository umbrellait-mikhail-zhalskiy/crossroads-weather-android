package com.umbrellait.weather.domain

sealed class Result<out T: Any> {
    data class Success<out T : Any>(val data: T) : Result<T>()
    data class Error(val message: String, val exception: Exception? = null, val code: Int? = null) : Result<Nothing>()
    object Loading: Result<Nothing>()
}
