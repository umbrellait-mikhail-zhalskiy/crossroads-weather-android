package com.umbrellait.weather.network.converter

import com.umbrellait.weather.domain.*
import com.umbrellait.weather.network.response.*

class NetworkToDomainConverter {

    fun toWeatherCityList(weatherCityListResponse: WeatherCityListResponse): List<WeatherCity> {
        return weatherCityListResponse.list.map { toWeatherCity(it) }
    }

    fun toWeatherCity(weatherCityResponse: WeatherCityResponse): WeatherCity {
        return WeatherCity(
            coord = toCoord(weatherCityResponse.coord),
            weather = toWeather(weatherCityResponse.weather.firstOrNull()),
            main = toMain(weatherCityResponse.main),
            wind = toWind(weatherCityResponse.wind),
            clouds = toClouds(weatherCityResponse.clouds),
            rain = toRain(weatherCityResponse.rain),
            snow = toSnow(weatherCityResponse.snow),
            dt = weatherCityResponse.dt,
            son = toSon(weatherCityResponse.sys),
            timezone = weatherCityResponse.timezone,
            id = weatherCityResponse.id,
            name = weatherCityResponse.name,
            visibility = weatherCityResponse.visibility
        )
    }

    private fun toCoord(coordResponse: CoordResponse): Coord {
        return Coord(
            lat = coordResponse.lat.toString(),
            lon = coordResponse.lon.toString()
        )
    }

    private fun toWeather(weatherResponse: WeatherResponse?): Weather {
        return Weather(
            id = weatherResponse?.id ?: 0,
            main = weatherResponse?.main ?: "",
            description = weatherResponse?.description ?: "",
            icon = weatherResponse?.icon ?: ""
        )
    }

    private fun toMain(mainResponse: MainResponse): Main {
        return Main(
            feelsLike = mainResponse.feelsLike,
            humidity = mainResponse.humidity,
            pressure = mainResponse.pressure,
            temp = mainResponse.temp.toInt()
        )
    }

    private fun toWind(windResponse: WindResponse) : Wind {
        return Wind(
            deg = windResponse.deg,
            speed = windResponse.speed
        )
    }

    private fun toClouds(cloudsResponse: CloudsResponse): Clouds {
        return Clouds(
            all = cloudsResponse.all
        )
    }

    private fun toRain(rainResponse: RainResponse?): Rain {
        return Rain(
            lastHour = rainResponse?.lastHour ?: 0.0
        )
    }

    private fun toSnow(snowResponse: SnowResponse?): Snow {
        return Snow(
            lastHour = snowResponse?.lastHour ?: 0.0
        )
    }

    private fun toSon(sysResponse: SysResponse): Son {
        return Son(
            sunrise = sysResponse.sunrise,
            sunset = sysResponse.sunset
        )
    }

    fun toDailyList(sevenDaysResponse: SevenDaysResponse): List<Daily> {
        val lat = sevenDaysResponse.lat.toString()
        val lon = sevenDaysResponse.lon.toString()
        return sevenDaysResponse.daily.map { toDaily(lat, lon, it) }.drop(1)
    }

    private fun toDaily(lat: String, lon: String, dailyResponse: DailyResponse): Daily {
        return Daily(
            lat = lat,
            lon = lon,
            clouds = dailyResponse.clouds,
            dt = dailyResponse.dt,
            feelsLike = toFeelsLike(dailyResponse.feelsLike),
            humidity = dailyResponse.humidity,
            pressure = dailyResponse.pressure,
            sunrise = dailyResponse.sunrise,
            sunset = dailyResponse.sunset,
            temp = toTemp(dailyResponse.temp),
            uvi = dailyResponse.uvi,
            weather = toWeather(dailyResponse.weather.firstOrNull()),
            windDeg = dailyResponse.windDeg,
            windSpeed = dailyResponse.windSpeed
        )
    }

    private fun toTemp(temp: TempResponse): Temp {
        return Temp(
            morn = temp.morn.toInt(),
            day = temp.day.toInt(),
            eve = temp.eve.toInt(),
            night = temp.night.toInt()
        )
    }

    private fun toFeelsLike(feelsLikeResponse: FeelsLikeResponse): FeelsLike {
        return FeelsLike(
            day = feelsLikeResponse.day.toInt(),
            eve = feelsLikeResponse.eve.toInt(),
            morn = feelsLikeResponse.morn.toInt(),
            night = feelsLikeResponse.night.toInt()
        )
    }


}