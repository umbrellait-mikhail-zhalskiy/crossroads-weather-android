package com.umbrellait.weather.network.interceptor

import com.umbrellait.weather.network.nameApiKey
import com.umbrellait.weather.network.valueApiKey
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class ApiKeyInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {

        val request = chain.request()

        val newRequest = when(request.tag(AuthorizedType::class.java)) {
            AuthorizedType.API_KEY -> request.addApiKey()
            else -> request
        }

        return chain.proceed(newRequest)
    }

    private fun Request.addApiKey(): Request {
        val newUrl = url.newBuilder().addQueryParameter(nameApiKey, valueApiKey).build()
        return newBuilder()
            .url(newUrl)
            .build()
    }
}