package com.umbrellait.weather.network.response

import com.google.gson.annotations.SerializedName

data class WeatherCityListResponse(
    @SerializedName("cnt") val cnt: Int,
    @SerializedName("list") val list: List<WeatherCityResponse>
)

data class WeatherCityResponse(
    @SerializedName("coord") val coord: CoordResponse,
    @SerializedName("weather") val weather: List<WeatherResponse>,
    @SerializedName("base") val base: String,
    @SerializedName("main") val main: MainResponse,
    @SerializedName("wind") val wind: WindResponse,
    @SerializedName("clouds") val clouds: CloudsResponse,
    @SerializedName("rain") val rain: RainResponse?,
    @SerializedName("snow") val snow: SnowResponse?,
    @SerializedName("dt") val dt: Long,
    @SerializedName("sys") val sys: SysResponse,
    @SerializedName("timezone") val timezone: Int,
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("cod") val cod: Int,
    @SerializedName("visibility") val visibility: Int
)

data class CloudsResponse(
    @SerializedName("all") val all: Int
)

data class RainResponse(
    @SerializedName("1h") val lastHour: Double,
    @SerializedName("3h") val lastThreeHour: Double
)

data class SnowResponse(
    @SerializedName("1h") val lastHour: Double,
    @SerializedName("3h") val lastThreeHour: Double
)

data class CoordResponse(
    @SerializedName("lat") val lat: Double,
    @SerializedName("lon") val lon: Double
)

data class MainResponse(
    @SerializedName("feels_like") val feelsLike: Double,
    @SerializedName("humidity") val humidity: Int,
    @SerializedName("pressure") val pressure: Int,
    @SerializedName("temp") val temp: Double,
    @SerializedName("temp_max") val tempMax: Double,
    @SerializedName("temp_min") val tempMin: Double
)

data class SysResponse(
    @SerializedName("country") val country: String,
    @SerializedName("id") val id: Int,
    @SerializedName("sunrise") val sunrise: Long,
    @SerializedName("sunset") val sunset: Long,
    @SerializedName("type") val type: Int
)

data class WindResponse(
    @SerializedName("speed") val speed: Float,
    @SerializedName("deg") val deg: Int
)