package com.umbrellait.weather.network

const val baseUri: String = "https://api.openweathermap.org/data/2.5/"

const val nameApiKey: String = "appid"

const val valueApiKey: String = "2f3c87d50da3ca8640a2ec7261724aa6"

const val WEATHER: String = "weather"

const val GROUP: String = "group"

const val ONECALL: String = "onecall"