package com.umbrellait.weather.network.response

import com.google.gson.annotations.SerializedName

data class SevenDaysResponse(
    @SerializedName("daily") val daily: List<DailyResponse>,
    @SerializedName("lat") val lat: Double,
    @SerializedName("lon") val lon: Double,
    @SerializedName("timezone") val timezone: String,
    @SerializedName("timezone_offset") val timezoneOffset: Int
)

data class DailyResponse(
    @SerializedName("clouds") val clouds: Int,
    @SerializedName("dew_point") val dewPoint: Double,
    @SerializedName("dt") val dt: Long,
    @SerializedName("feels_like") val feelsLike: FeelsLikeResponse,
    @SerializedName("humidity") val humidity: Int,
    @SerializedName("pop") val pop: Double,
    @SerializedName("pressure") val pressure: Int,
    @SerializedName("rain") val rain: Double,
    @SerializedName("sunrise") val sunrise: Long,
    @SerializedName("sunset") val sunset: Long,
    @SerializedName("temp") val temp: TempResponse,
    @SerializedName("uvi") val uvi: Double,
    @SerializedName("weather") val weather: List<WeatherResponse>,
    @SerializedName("wind_deg") val windDeg: Int,
    @SerializedName("wind_speed") val windSpeed: Double
)

data class FeelsLikeResponse(
    @SerializedName("day") val day: Double,
    @SerializedName("eve") val eve: Double,
    @SerializedName("morn") val morn: Double,
    @SerializedName("night") val night: Double
)

data class TempResponse(
    @SerializedName("day") val day: Double,
    @SerializedName("eve") val eve: Double,
    @SerializedName("max") val max: Double,
    @SerializedName("min") val min: Double,
    @SerializedName("morn") val morn: Double,
    @SerializedName("night") val night: Double
)


/*
{
    "lat": 55.75,
    "lon": 37.62,
    "timezone": "Europe/Moscow",
    "timezone_offset": 10800,
    "daily": [
        {
            "dt": 1602493200,
            "sunrise": 1602474869,
            "sunset": 1602513424,
            "temp": {
                "day": 12.76,
                "min": 9.98,
                "max": 12.76,
                "night": 12.46,
                "eve": 12.01,
                "morn": 9.98
            },
            "feels_like": {
                "day": 10.36,
                "night": 11.51,
                "eve": 10.62,
                "morn": 7.04
            },
            "pressure": 1017,
            "humidity": 77,
            "dew_point": 8.83,
            "wind_speed": 3.05,
            "wind_deg": 185,
            "weather": [
                {
                    "id": 501,
                    "main": "Rain",
                    "description": "moderate rain",
                    "icon": "10d"
                }
            ],
            "clouds": 96,
            "pop": 0.7,
            "rain": 3.21,
            "uvi": 1.44
        },
        {
            "dt": 1602579600,
            "sunrise": 1602561391,
            "sunset": 1602599673,
            "temp": {
                "day": 14.47,
                "min": 10.94,
                "max": 15.64,
                "night": 12.41,
                "eve": 13.81,
                "morn": 10.94
            },
            "feels_like": {
                "day": 12.69,
                "night": 11.59,
                "eve": 12.96,
                "morn": 9.88
            },
            "pressure": 1018,
            "humidity": 71,
            "dew_point": 9.29,
            "wind_speed": 2.33,
            "wind_deg": 241,
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "clouds": 91,
            "pop": 0,
            "uvi": 1.46
        },
        {
            "dt": 1602666000,
            "sunrise": 1602647912,
            "sunset": 1602685923,
            "temp": {
                "day": 12.75,
                "min": 9.94,
                "max": 13.33,
                "night": 11.11,
                "eve": 11.86,
                "morn": 9.94
            },
            "feels_like": {
                "day": 10.13,
                "night": 7.89,
                "eve": 8.69,
                "morn": 7.81
            },
            "pressure": 1020,
            "humidity": 77,
            "dew_point": 8.9,
            "wind_speed": 3.37,
            "wind_deg": 63,
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "clouds": 96,
            "pop": 0,
            "uvi": 1.45
        },
        {
            "dt": 1602752400,
            "sunrise": 1602734434,
            "sunset": 1602772174,
            "temp": {
                "day": 12.69,
                "min": 10.05,
                "max": 12.87,
                "night": 10.05,
                "eve": 11.53,
                "morn": 10.93
            },
            "feels_like": {
                "day": 10.59,
                "night": 4.56,
                "eve": 6.61,
                "morn": 7.8
            },
            "pressure": 1010,
            "humidity": 82,
            "dew_point": 9.81,
            "wind_speed": 2.95,
            "wind_deg": 181,
            "weather": [
                {
                    "id": 502,
                    "main": "Rain",
                    "description": "heavy intensity rain",
                    "icon": "10d"
                }
            ],
            "clouds": 98,
            "pop": 1,
            "rain": 14.38,
            "uvi": 1.36
        },
        {
            "dt": 1602838800,
            "sunrise": 1602820957,
            "sunset": 1602858426,
            "temp": {
                "day": 10.85,
                "min": 7.79,
                "max": 10.85,
                "night": 7.79,
                "eve": 8.88,
                "morn": 8.77
            },
            "feels_like": {
                "day": 7.49,
                "night": 4.04,
                "eve": 5.78,
                "morn": 4.06
            },
            "pressure": 1010,
            "humidity": 71,
            "dew_point": 5.85,
            "wind_speed": 3.43,
            "wind_deg": 269,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "clouds": 93,
            "pop": 0.4,
            "rain": 0.6,
            "uvi": 1.29
        },
        {
            "dt": 1602925200,
            "sunrise": 1602907480,
            "sunset": 1602944679,
            "temp": {
                "day": 4.63,
                "min": 4.63,
                "max": 6.81,
                "night": 5.62,
                "eve": 5.38,
                "morn": 5.5
            },
            "feels_like": {
                "day": -1.5,
                "night": 1.39,
                "eve": 0.42,
                "morn": 0.14
            },
            "pressure": 1001,
            "humidity": 86,
            "dew_point": 2.55,
            "wind_speed": 6.48,
            "wind_deg": 72,
            "weather": [
                {
                    "id": 502,
                    "main": "Rain",
                    "description": "heavy intensity rain",
                    "icon": "10d"
                }
            ],
            "clouds": 100,
            "pop": 1,
            "rain": 25.27,
            "uvi": 1.1
        },
        {
            "dt": 1603011600,
            "sunrise": 1602994003,
            "sunset": 1603030932,
            "temp": {
                "day": 1.96,
                "min": 0.26,
                "max": 5.86,
                "night": 0.26,
                "eve": 1.09,
                "morn": 2.96
            },
            "feels_like": {
                "day": -5.41,
                "night": -6.22,
                "eve": -5.88,
                "morn": -4.51
            },
            "pressure": 999,
            "humidity": 84,
            "dew_point": -1.38,
            "wind_speed": 7.6,
            "wind_deg": 333,
            "weather": [
                {
                    "id": 501,
                    "main": "Rain",
                    "description": "moderate rain",
                    "icon": "10d"
                }
            ],
            "clouds": 100,
            "pop": 1,
            "rain": 8.88,
            "uvi": 0.99
        },
        {
            "dt": 1603098000,
            "sunrise": 1603080526,
            "sunset": 1603117187,
            "temp": {
                "day": 0.8,
                "min": -0.04,
                "max": 1.26,
                "night": 1.26,
                "eve": 1.26,
                "morn": -0.04
            },
            "feels_like": {
                "day": -5.44,
                "night": -5.08,
                "eve": -5.08,
                "morn": -6.11
            },
            "pressure": 1012,
            "humidity": 87,
            "dew_point": -4.14,
            "wind_speed": 5.85,
            "wind_deg": 288,
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "clouds": 100,
            "pop": 0.08,
            "uvi": 1
        }
    ]
}
 */