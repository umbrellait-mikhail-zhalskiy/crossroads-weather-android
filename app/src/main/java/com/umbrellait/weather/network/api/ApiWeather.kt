package com.umbrellait.weather.network.api

import com.umbrellait.weather.network.GROUP
import com.umbrellait.weather.network.ONECALL
import com.umbrellait.weather.network.interceptor.AuthorizedType
import com.umbrellait.weather.network.WEATHER
import com.umbrellait.weather.network.response.SevenDaysResponse
import com.umbrellait.weather.network.response.WeatherCityResponse
import com.umbrellait.weather.network.response.WeatherCityListResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Tag

interface ApiWeather {

    @GET(WEATHER)
    suspend fun getWeatherCity(
        @Query("q") nameCity: String,
        @Query("lang") lang: String = "ru",
        @Query("units") unit: String = "metric",
        @Tag authorizedType: AuthorizedType = AuthorizedType.API_KEY
    ): Response<WeatherCityResponse>

    @GET(GROUP)
    suspend fun getWeatherCityList(
        @Query("id") idList: String,
        @Query("lang") lang: String = "ru",
        @Query("units") unit: String = "metric",
        @Tag authorizedType: AuthorizedType = AuthorizedType.API_KEY
    ): Response<WeatherCityListResponse>

    @GET(ONECALL)
    suspend fun getWeatherCityInSevenDays(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("exclude") exclude: String = "current,minutely,hourly,alerts",
        @Query("lang") lang: String = "ru",
        @Query("units") unit: String = "metric",
        @Tag authorizedType: AuthorizedType = AuthorizedType.API_KEY
    ): Response<SevenDaysResponse>
}