package com.umbrellait.weather.network

import com.umbrellait.weather.domain.Daily
import com.umbrellait.weather.domain.WeatherCity
import com.umbrellait.weather.network.api.ApiWeather
import com.umbrellait.weather.network.converter.NetworkToDomainConverter
import com.umbrellait.weather.network.response.SevenDaysResponse
import com.umbrellait.weather.network.response.WeatherCityResponse
import com.umbrellait.weather.network.response.WeatherCityListResponse
import retrofit2.Response

class NetworkImpl(
    private val apiWeather: ApiWeather,
    private val networkToDomainConverter: NetworkToDomainConverter
) : Network {

    override suspend fun getWeatherCity(nameCity: String): NetworkResult<WeatherCity> {
        return try {
            val response: Response<WeatherCityResponse> =
                apiWeather.getWeatherCity(nameCity = nameCity)
            return if (response.isSuccessful) {
                response.body()?.let {
                    NetworkResult.Success(
                        networkToDomainConverter.toWeatherCity(it)
                    )
                } ?: NetworkResult.Error("error getWeatherCity() -> responseBody == null")
            } else {
                NetworkResult.Error(response.message(), code = response.code())
            }
        } catch (e: Exception) {
            NetworkResult.Error("error ${e.message}")
        }
    }

    override suspend fun getWeatherCityList(idList: String): NetworkResult<List<WeatherCity>> {
        return try {
            val response: Response<WeatherCityListResponse> =
                apiWeather.getWeatherCityList(idList = idList)
            return if (response.isSuccessful) {
                response.body()?.let {
                    NetworkResult.Success(
                        networkToDomainConverter.toWeatherCityList(it)
                    )
                } ?: NetworkResult.Error("error getWeatherCityList() -> responseBody == null")
            } else {
                NetworkResult.Error(response.message())
            }
        } catch (e: Exception) {
            NetworkResult.Error("error ${e.message}")
        }
    }

    override suspend fun getWeatherCityInSevenDays(
        lat: String,
        lon: String
    ): NetworkResult<List<Daily>> {
        return try {
            val response: Response<SevenDaysResponse> =
                apiWeather.getWeatherCityInSevenDays(lat = lat.toDouble(), lon = lon.toDouble())
            return if (response.isSuccessful) {
                response.body()?.let {
                    NetworkResult.Success(
                        networkToDomainConverter.toDailyList(it)
                    )
                } ?: NetworkResult.Error("error getWeatherCityInSevenDays() -> responseBody == null")
            } else {
                NetworkResult.Error(response.message())
            }
        } catch (e: Exception) {
            NetworkResult.Error("error ${e.message}")
        }
    }
}


