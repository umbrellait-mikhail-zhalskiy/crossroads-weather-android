package com.umbrellait.weather.ui.search

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import androidx.core.view.forEach
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.chip.Chip
import com.google.android.material.textfield.TextInputEditText
import com.umbrellait.weather.R
import com.umbrellait.weather.databinding.SearchFragmentBinding
import com.umbrellait.weather.domain.items.EmptyState
import com.umbrellait.weather.utils.setThrottledClickListener
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import org.koin.androidx.viewmodel.ext.android.viewModel


@FlowPreview
@ExperimentalCoroutinesApi
class SearchFragment : Fragment() {

    private var _binding: SearchFragmentBinding? = null
    private val binding get() = _binding!!

    private val searchViewModel: SearchViewModel by viewModel()

    private val searchAdapter: SearchAdapter = SearchAdapter {
        lifecycleScope.launchWhenStarted {
            searchViewModel.sendSearchViewEvent(
                SearchViewEvent.AddCityDataEvent(
                    binding.searchTextInput.editText?.text.toString()
                )
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = SearchFragmentBinding.inflate(layoutInflater)
        return binding.root
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        searchViewModel.state
            .onEach { handlerState(it) }
            .launchIn(lifecycleScope)

        binding.chipGroup.forEach {
            if(it is Chip) {
                it.setOnCheckedChangeListener { _, isChecked ->
                    it.setTextColor(
                        ContextCompat.getColor(
                            requireContext(),
                            if(isChecked)
                                android.R.color.white
                            else
                                R.color.colorBlack)
                    )
                    if(it.isChecked) {
                        binding.searchTextInput.editText?.setText(it.text)
                    }
                }
            }
        }

        binding.searchRecyclerView.adapter = searchAdapter

        binding.searchRecyclerView.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)

        binding.searchTextInput.editText?.setOnTouchListener { view, motionEvent ->
            val DRAWABLE_RIGHT = 2
            if(motionEvent.action == MotionEvent.ACTION_UP) {
                if(motionEvent.rawX >= (view.right - (view as TextInputEditText).compoundDrawables[DRAWABLE_RIGHT].bounds.width())) {
                    view.setText("")
                    searchAdapter.items = listOf()
                    true
                }
            }
            false
        }

        binding.searchTextInput.editText?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if(p3 > 1)
                    binding.searchTextInput.editText?.setSelection(p3)

                lifecycleScope.launchWhenStarted { searchViewModel.sendSearchViewEvent(SearchViewEvent.SearchDataEvent(p0.toString())) }
            }

            override fun afterTextChanged(p0: Editable?) {
            }
        })

        binding.backArrow.setThrottledClickListener {
            findNavController().popBackStack()
        }
    }

    override fun onDestroyView() {
        val imm =
            requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm!!.hideSoftInputFromWindow(requireView().windowToken, 0)
        super.onDestroyView()
        _binding = null
    }

    private fun handlerState(searchViewState: SearchViewState) {
        when(searchViewState.searchView) {
            is SearchView.Loading -> {
                binding.searchProgressBar.visibility = View.VISIBLE
            }
            is SearchView.Error -> {
                binding.searchProgressBar.visibility = View.GONE
                searchAdapter.items = searchViewState.searchView.list
                searchAdapter.notifyDataSetChanged()
            }
            is SearchView.Empty -> {
                binding.searchProgressBar.visibility = View.GONE
                searchAdapter.items = listOf()
                searchAdapter.notifyDataSetChanged()
            }
            is SearchView.Success -> {
                binding.searchProgressBar.visibility = View.GONE
                searchAdapter.items = searchViewState.searchView.list
                searchAdapter.notifyDataSetChanged()
            }
        }

        when(searchViewState.addCityView) {
            is AddCityView.Success -> {
                findNavController().navigateUp()
            }
        }
    }

}