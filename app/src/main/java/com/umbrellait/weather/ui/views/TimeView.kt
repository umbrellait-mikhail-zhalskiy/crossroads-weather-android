package com.umbrellait.weather.ui.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.umbrellait.weather.R
import com.umbrellait.weather.databinding.ViewTimeBinding

class TimeView(context: Context, attributeSet: AttributeSet): ConstraintLayout(context, attributeSet) {

    private var _binding: ViewTimeBinding? = null
    private val binding get() = _binding!!

    init {
        View.inflate(context, R.layout.view_time, this)
        _binding = ViewTimeBinding.bind(this)

        val attributes = context.obtainStyledAttributes(attributeSet, R.styleable.TimeView)
        binding.timeTextView.text = attributes.getString(R.styleable.TimeView_time_text)
        binding.gradeTextView.text = attributes.getString(R.styleable.TimeView_degree_text)
        binding.timeImageView.setImageResource(attributes.getResourceId(R.styleable.CategoryView_icon, R.drawable.ic_few_clouds_day))

        attributes.recycle()
    }

    fun setText(text: String) {
        binding.gradeTextView.text = text
    }

    fun setIcon(resId: Int) {
        binding.timeImageView.setImageResource(resId)
    }
}