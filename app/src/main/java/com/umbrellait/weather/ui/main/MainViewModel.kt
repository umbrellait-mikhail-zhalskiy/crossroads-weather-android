package com.umbrellait.weather.ui.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.umbrellait.weather.domain.Result
import com.umbrellait.weather.repository.Repository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*

@FlowPreview
@ExperimentalCoroutinesApi
class MainViewModel(
    private val repository: Repository
) : ViewModel() {

    private val eventsChannel = BroadcastChannel<MainViewEvent>(capacity = Channel.CONFLATED)

    private val _state = MutableStateFlow(MainViewState())
    val state: StateFlow<MainViewState>
        get() = _state

    private val eventsFlow = eventsChannel.asFlow()

    private val loadDataFlow = eventsFlow.filterIsInstance<MainViewEvent.LoadDataEvent>()
        .flatMapLatest {
            flowOf(repository.getWeatherCityList())
                .onStart { emit(Result.Loading) }
        }
        .map { MainView.toMainView(it) }
        .map { MainView.toPartialViewState(it) }

    init {
        merge(loadDataFlow)
            .scan(MainViewState(), { accumulator, value -> value.invoke(accumulator) })
            .onEach { _state.value = it }
            .catch {  }
            .launchIn(viewModelScope)
    }

    suspend fun sendMainViewEvent(mainViewEvent: MainViewEvent) {
        eventsChannel.send(mainViewEvent)
    }

//    fun loadWeather() {
//        viewModelScope.launch {
//            flowOf(repository.getWeatherCity("Минск"))
//            flowOf(repository.getWeatherCity("Москва"))
//        }
//    }
}
