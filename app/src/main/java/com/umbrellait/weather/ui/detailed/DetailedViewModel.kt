package com.umbrellait.weather.ui.detailed

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.umbrellait.weather.domain.Result
import com.umbrellait.weather.repository.Repository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*

@FlowPreview
@ExperimentalCoroutinesApi
class DetailedViewModel(
    private val nameCity: String,
    private val repository: Repository
) : ViewModel() {

    private val eventsChannel = BroadcastChannel<DetailedViewEvent>(capacity = Channel.CONFLATED)

    private val _state = MutableStateFlow(DetailedViewState())
    val state: StateFlow<DetailedViewState>
        get() = _state

    private val eventsFlow = eventsChannel.asFlow()

    private val loadDataFlow = eventsFlow.filterIsInstance<DetailedViewEvent.LoadDataEvent>()
        .flatMapLatest {
            flowOf(repository.getWeatherCity(nameCity))
                .onStart { emit(Result.Loading) }
        }
        .map { DetailedView.toDetailedView(it) }
        .map { DetailedView.toPartialViewState(it) }

    init {
        merge(loadDataFlow)
            .scan(DetailedViewState(), { accumulator, value -> value.invoke(accumulator) })
            .onEach { _state.value = it }
            .launchIn(viewModelScope)
    }

    suspend fun sendDetailedViewEvent(detailedViewEvent: DetailedViewEvent) {
        eventsChannel.send(detailedViewEvent)
    }
}