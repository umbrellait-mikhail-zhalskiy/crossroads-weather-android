package com.umbrellait.weather.ui.detailed

import com.umbrellait.weather.domain.Result
import com.umbrellait.weather.domain.WeatherCity
import com.umbrellait.weather.domain.items.CardState
import com.umbrellait.weather.utils.CardSource

private typealias PartialViewState = (DetailedViewState) -> DetailedViewState

data class DetailedViewState(
    val detailedView: DetailedView = DetailedView.Loading
)

sealed class DetailedView {
    data class Success(val cardState: CardState): DetailedView()
    object Error: DetailedView()
    object Loading: DetailedView()

    companion object {
        fun toDetailedView(result: Result<WeatherCity>): DetailedView =
            when(result) {
                is Result.Loading -> Loading
                is Result.Error -> Error
                is Result.Success -> Success(
                    CardState(weatherCity = result.data, daily = null, cardSource = CardSource.DETAIL))
            }

        fun toPartialViewState(detailedView: DetailedView): PartialViewState =
            { previousViewState ->
                previousViewState.copy(detailedView = detailedView)
            }
    }
}

sealed class DetailedViewEvent {
    object LoadDataEvent: DetailedViewEvent()
}