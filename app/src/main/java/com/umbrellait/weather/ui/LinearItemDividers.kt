package com.umbrellait.weather.ui

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.view.View
import androidx.annotation.ColorInt
import androidx.recyclerview.widget.RecyclerView

class LinearItemDividers(
    private val divSize: Int,
    private val horizontalMargin: Int,
    @ColorInt private val color: Int,
    private val needDrawDiv: (position: Int) -> Boolean = {true}
): RecyclerView.ItemDecoration() {

    private val paint = Paint().apply {
        color = this@LinearItemDividers.color
    }

    override fun onDrawOver(
        c: Canvas,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        for (i in 0 until parent.childCount) {
            val child = parent.getChildAt(i)
            if (needDrawDivider(child, parent)) {
                c.drawRect(
                    Rect(child.left, child.top - divSize, child.width, child.top),
                    paint
                )
            }
        }
    }

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        if (needDrawDivider(view, parent)) {
            outRect.top = divSize
            outRect.left = horizontalMargin
            outRect.right = horizontalMargin
        }
    }

    private fun needDrawDivider(view: View, parent: RecyclerView): Boolean {
        val itemPosition = parent.getChildAdapterPosition(view)
        if (itemPosition == RecyclerView.NO_POSITION) return false
        val itemCount = parent.adapter?.itemCount ?: 0
        if (itemCount == 0) return false

        if (itemPosition in 0 until itemCount) {
            return needDrawDiv(itemPosition)
        }
        return false
    }
}