package com.umbrellait.weather.ui.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.umbrellait.weather.R
import com.umbrellait.weather.databinding.ViewCategoryWeatherBinding

class CategoryView(context: Context, attributeSet: AttributeSet): ConstraintLayout(context, attributeSet){
    private var _binding: ViewCategoryWeatherBinding? = null
    private val binding get() = _binding!!

    init {
        View.inflate(context, R.layout.view_category_weather, this)
        _binding = ViewCategoryWeatherBinding.bind(this)

        val attributes = context.obtainStyledAttributes(attributeSet, R.styleable.CategoryView)

//        binding.verticalSeparator.visibility =
//            if(attributes.getBoolean(R.styleable.CategoryView_left_separator_visible, false))
//                View.VISIBLE
//            else
//                View.GONE

        binding.nameTextView.text = attributes.getString(R.styleable.CategoryView_category_name)
        binding.valueTextView.text = attributes.getString(R.styleable.CategoryView_value_text)
        binding.categoryImageView.setImageResource(attributes.getResourceId(R.styleable.CategoryView_icon, R.drawable.ic_wind))
        attributes.recycle()
    }

    fun setText(text: String) {
        binding.valueTextView.text = text
    }
}