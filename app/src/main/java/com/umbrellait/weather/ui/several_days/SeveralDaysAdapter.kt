package com.umbrellait.weather.ui.several_days

import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import com.umbrellait.weather.domain.items.DisplayableItem
import com.umbrellait.weather.ui.delegate.cardAdapterDelegate
import com.umbrellait.weather.ui.delegate.errorAdapterDelegate

class SeveralDaysAdapter: ListDelegationAdapter<List<DisplayableItem>>(
    cardAdapterDelegate(),
    errorAdapterDelegate()
)