package com.umbrellait.weather.ui

import com.umbrellait.weather.ui.detailed.DetailedViewModel
import com.umbrellait.weather.ui.main.MainViewModel
import com.umbrellait.weather.ui.search.SearchViewModel
import com.umbrellait.weather.ui.several_days.SeveralDaysViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

@FlowPreview
@ExperimentalCoroutinesApi
val uiKoinModule: Module = module {

    viewModel {
        MainViewModel(
            repository = get(),
        )
    }

    viewModel { (nameCity: String) ->
        DetailedViewModel(nameCity = nameCity, repository = get())
    }

    viewModel {(lat: String, lon: String) ->
        SeveralDaysViewModel(
            lat = lat,
            lon = lon,
            repository = get()
        )
    }

    viewModel {
        SearchViewModel(
            repository = get(),
        )
    }
}