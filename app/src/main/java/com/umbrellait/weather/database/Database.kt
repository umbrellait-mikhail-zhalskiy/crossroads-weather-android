package com.umbrellait.weather.database

import com.umbrellait.weather.domain.Daily
import com.umbrellait.weather.domain.WeatherCity

interface Database {

    suspend fun insertOrUpdate(weatherCity: WeatherCity)

    suspend fun getWeatherCityList(): List<WeatherCity>

    suspend fun getDailyList(lat: String, lon: String): List<Daily>

    suspend fun updateDailyList(dailyListOld: List<Daily>, dailyListNew: List<Daily>)
}