package com.umbrellait.weather.database

import com.umbrellait.weather.database.converter.DatabaseToDomainConverter
import com.umbrellait.weather.database.converter.DomainToDatabaseConverter
import com.umbrellait.weather.database.dao.WeatherDatabase
import com.umbrellait.weather.domain.Daily
import com.umbrellait.weather.domain.WeatherCity

class DatabaseImpl(
    private val weatherDatabase: WeatherDatabase,
    private val databaseToDomainConverter: DatabaseToDomainConverter,
    private val domainToDatabaseConverter: DomainToDatabaseConverter
) : Database {

    override suspend fun insertOrUpdate(weatherCity: WeatherCity) {
        weatherDatabase.weatherDao.insert(domainToDatabaseConverter.toWeatherCityEntity(weatherCity))
    }

    override suspend fun getWeatherCityList(): List<WeatherCity> {
        return weatherDatabase.weatherDao.getWeatherCityList()
            .map { databaseToDomainConverter.toWeatherCity(it) }
    }

    override suspend fun getDailyList(lat: String, lon: String): List<Daily> {
        return weatherDatabase.weatherDao.getDailyList(lat = lat, lon = lon)
            .map { databaseToDomainConverter.toDaily(it) }
    }

    override suspend fun updateDailyList(dailyListOld: List<Daily>, dailyListNew: List<Daily>) {
        weatherDatabase.weatherDao.delete(domainToDatabaseConverter.toDailyEntityList(dailyListOld))
        weatherDatabase.weatherDao.insert(domainToDatabaseConverter.toDailyEntityList(dailyListNew))
    }
}