package com.umbrellait.weather.database.di

import android.app.Application
import androidx.room.Room
import com.umbrellait.weather.database.Database
import com.umbrellait.weather.database.DatabaseImpl
import com.umbrellait.weather.database.converter.DatabaseToDomainConverter
import com.umbrellait.weather.database.converter.DomainToDatabaseConverter
import com.umbrellait.weather.database.dao.WeatherDatabase
import org.koin.android.ext.koin.androidApplication
import org.koin.core.module.Module
import org.koin.dsl.bind
import org.koin.dsl.module

val databaseKoinModule: Module = module {

    single { provideDatabase(androidApplication()) }

    single { provideDatabaseToDomainConverter() }

    single { provideDomainToDatabaseConverter() }

    single {
        DatabaseImpl(
            weatherDatabase = get(),
            databaseToDomainConverter = get(),
            domainToDatabaseConverter = get()
        )
    } bind (Database::class)
}

fun provideDatabase(application: Application): WeatherDatabase {
    return Room.databaseBuilder(application, WeatherDatabase::class.java, "weather.db")
        .build()
}

fun provideDatabaseToDomainConverter() = DatabaseToDomainConverter()

fun provideDomainToDatabaseConverter() = DomainToDatabaseConverter()