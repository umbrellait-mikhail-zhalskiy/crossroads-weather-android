package com.umbrellait.weather.database.dao

import androidx.room.*
import com.umbrellait.weather.database.entity.DailyEntity
import com.umbrellait.weather.database.entity.WeatherCityEntity


@Dao
interface WeatherDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(weatherCityEntity: WeatherCityEntity)

    @Query("SELECT * FROM weather_city ORDER BY id DESC")
    suspend fun getWeatherCityList(): List<WeatherCityEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(dailyEntity: DailyEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(dailyEntityList: List<DailyEntity>)

    @Delete
    suspend fun delete(dailyEntityList: List<DailyEntity>): Int

    @Query("SELECT * FROM daily WHERE lat == :lat AND lon == :lon ORDER BY dt ASC")
    suspend fun getDailyList(lat: String, lon: String): List<DailyEntity>
}