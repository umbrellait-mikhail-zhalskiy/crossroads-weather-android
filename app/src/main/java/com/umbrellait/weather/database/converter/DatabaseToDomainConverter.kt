package com.umbrellait.weather.database.converter

import com.umbrellait.weather.database.entity.*
import com.umbrellait.weather.domain.*

class DatabaseToDomainConverter {

    fun toWeatherCity(weatherCityEntity: WeatherCityEntity): WeatherCity {
        return WeatherCity(
            coord = toCoord(weatherCityEntity.coord),
            weather = toWeather(weatherCityEntity.weather),
            main = toMain(weatherCityEntity.main),
            wind = toWind(weatherCityEntity.wind),
            clouds = toClouds(weatherCityEntity.clouds),
            rain = toRain(weatherCityEntity.rain),
            snow = toSnow(weatherCityEntity.snow),
            dt = weatherCityEntity.dt,
            son = toSon(weatherCityEntity.son),
            timezone = weatherCityEntity.timezone,
            id = weatherCityEntity.id,
            name = weatherCityEntity.name,
            visibility = weatherCityEntity.visibility
        )
    }

    private fun toCoord(coordEntity: CoordEntity): Coord {
        return Coord(
            lat = coordEntity.lat,
            lon = coordEntity.lon
        )
    }

    private fun toWeather(weatherEntity: WeatherEntity): Weather {
        return Weather(
            id = weatherEntity.id,
            main = weatherEntity.main,
            description = weatherEntity.description,
            icon = weatherEntity.icon
        )
    }

    private fun toMain(mainEntity: MainEntity): Main {
        return Main(
            feelsLike = mainEntity.feelsLike,
            humidity = mainEntity.humidity,
            pressure = mainEntity.pressure,
            temp = mainEntity.temp
        )
    }

    private fun toWind(windEntity: WindEntity): Wind {
        return Wind(
            deg = windEntity.deg,
            speed = windEntity.speed
        )
    }

    private fun toClouds(cloudsEntity: CloudsEntity): Clouds {
        return Clouds(
            all = cloudsEntity.all
        )
    }

    private fun toRain(rainEntity: RainEntity): Rain {
        return Rain(
            lastHour = rainEntity.lastHour
        )
    }

    private fun toSnow(snowEntity: SnowEntity): Snow {
        return Snow(
            lastHour = snowEntity.lastHour
        )
    }

    private fun toSon(sonEntity: SonEntity): Son {
        return Son(
            sunset = sonEntity.sunset,
            sunrise = sonEntity.sunrise
        )
    }

    fun toDaily(dailyEntity: DailyEntity): Daily {
        return Daily(
            lat = dailyEntity.lat,
            lon = dailyEntity.lon,
            clouds = dailyEntity.clouds,
            dt = dailyEntity.dt,
            feelsLike = toFeelsLike(dailyEntity.feelsLike),
            humidity = dailyEntity.humidity,
            pressure = dailyEntity.pressure,
            sunrise = dailyEntity.sunrise,
            sunset = dailyEntity.sunset,
            temp = toTemp(dailyEntity.temp),
            uvi = dailyEntity.uvi,
            weather = toWeather(dailyEntity.weather),
            windDeg = dailyEntity.windDeg,
            windSpeed = dailyEntity.windSpeed
        )
    }

    private fun toTemp(tempEntity: TempEntity): Temp {
        return Temp(
            morn = tempEntity.morn,
            day = tempEntity.day,
            eve = tempEntity.eve,
            night = tempEntity.night
        )
    }

    private fun toFeelsLike(feelsLikeEntity: FeelsLikeEntity): FeelsLike {
        return FeelsLike(
            morn = feelsLikeEntity.morn,
            day = feelsLikeEntity.day,
            eve = feelsLikeEntity.eve,
            night = feelsLikeEntity.night
        )
    }
}