package com.umbrellait.weather.database.entity

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "daily")
data class DailyEntity(
    @ColumnInfo(name = "lat") val lat: String,
    @ColumnInfo(name = "lon") val lon: String,
    @ColumnInfo(name = "clouds") val clouds: Int,
    @ColumnInfo(name = "dt") val dt: Long,
    @Embedded(prefix = "feels_like_") val feelsLike: FeelsLikeEntity,
    @ColumnInfo(name = "humidity") val humidity: Int,
    @ColumnInfo(name = "pressure") val pressure: Int,
    @ColumnInfo(name = "sunrise") val sunrise: Long,
    @ColumnInfo(name = "sunset") val sunset: Long,
    @Embedded(prefix = "temp_") val temp: TempEntity,
    @ColumnInfo(name = "uvi") val uvi: Double,
    @Embedded(prefix = "weather_") val weather: WeatherEntity,
    @ColumnInfo(name = "wind_deg") val windDeg: Int,
    @ColumnInfo(name = "wind_speed") val windSpeed: Double
) {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id") var id: Long = 0
}

data class FeelsLikeEntity(
    @ColumnInfo(name = "morn") val morn: Int,
    @ColumnInfo(name = "day") val day: Int,
    @ColumnInfo(name = "eve") val eve: Int,
    @ColumnInfo(name = "night") val night: Int
)

data class TempEntity(
    @ColumnInfo(name = "morn") val morn: Int,
    @ColumnInfo(name = "day") val day: Int,
    @ColumnInfo(name = "eve") val eve: Int,
    @ColumnInfo(name = "night") val night: Int
)