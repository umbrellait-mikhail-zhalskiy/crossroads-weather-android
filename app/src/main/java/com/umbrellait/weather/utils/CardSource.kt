package com.umbrellait.weather.utils

enum class CardSource {
    SEARCH,
    DETAIL,
    SEVERAL
}