package com.umbrellait.weather.utils

import android.content.res.Resources
import android.view.View
import com.umbrellait.weather.R

fun Int.toWindDirectionString(): String = when(this) {
    in 0..22 -> "ССВ"
    in 23..45 -> "СВ"
    in 46..67 -> "ВВС"
    in 67..90 -> "В"
    in 91..112 -> "ВЮВ"
    in 113..135 -> "ЮВ"
    in 136..157 -> "ЮЮВ"
    in 158..180 -> "Ю"
    in 181..202 -> "ЮЮЗ"
    in 203..225 -> "ЮЗ"
    in 226..247 -> "ЗЮЗ"
    in 226..247 -> "ЗЮЗ"
    in 226..247 -> "ЗЮЗ"
    in 248..270 -> "З"
    in 271..292 -> "ЗСЗ"
    in 293..315 -> "СЗ"
    in 316..337 -> "ССЗ"
    in 338..360 -> "ССЗ"
    else -> ""
}

fun Long.toHoursAndMinutesTime(): String = android.text.format.DateFormat.format("HH:mm", this).toString()

fun String.toWeatherIcon(): Int = when(this) {
    "01d" -> R.drawable.ic_clear_sky_day
    "02d" -> R.drawable.ic_few_clouds_day
    "03d" -> R.drawable.ic_scattered_clouds_day
    "04d" -> R.drawable.ic_broken_clouds_day
    "09d" -> R.drawable.ic_shower_rain_day
    "10d" -> R.drawable.ic_rain_day
    "11d" -> R.drawable.ic_thunderstorm_day
    "13d" -> R.drawable.ic_snow_day
    "50d" -> R.drawable.ic_mist_day

    "01n" -> R.drawable.ic_clear_sky_night
    "02n" -> R.drawable.ic_few_clouds_night
    "03n" -> R.drawable.ic_scattered_clouds_day
    "04n" -> R.drawable.ic_broken_clouds_day
    "09n" -> R.drawable.ic_shower_rain_day
    "10n" -> R.drawable.ic_rain_night
    "11n" -> R.drawable.ic_thunderstorm_day
    "13n" -> R.drawable.ic_snow_day
    "50n" -> R.drawable.ic_mist_day

    else -> R.drawable.ic_clear_sky_day
}

val Int.dpToPx: Int get() = (this * Resources.getSystem().displayMetrics.density).toInt()

private var lastClickTimestamp = 0L
fun View.setThrottledClickListener(delay: Long = 300L, clickListener: ((View) -> Unit)?) {
    if (clickListener != null) {
        setOnClickListener {
            val currentTimestamp = System.currentTimeMillis()
            val delta = currentTimestamp - lastClickTimestamp
            //0L for hidden bug: launch app -> move system time back -> return to app ...
            if (delta !in 0L..delay) {
                lastClickTimestamp = currentTimestamp
                clickListener(this)
            }
        }
    } else {
        setOnClickListener(null)
    }
}