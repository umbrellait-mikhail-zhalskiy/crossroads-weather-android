package com.umbrellait.weather.repository

import com.umbrellait.weather.domain.Daily
import com.umbrellait.weather.domain.Result
import com.umbrellait.weather.domain.WeatherCity

interface Repository {
    suspend fun getWeatherCity(nameCity: String, isSaveDatabase: Boolean = true): Result<WeatherCity>

    suspend fun getWeatherCityList(): Result<List<WeatherCity>>

    suspend fun getDailyList(lat: String, lon: String): Result<List<Daily>>
}