package com.umbrellait.weather.repository.di

import com.umbrellait.weather.repository.Repository
import com.umbrellait.weather.repository.RepositoryImpl
import org.koin.core.module.Module
import org.koin.dsl.bind
import org.koin.dsl.module

val repositoryKoinModule: Module = module {
    single {
        RepositoryImpl(
            network = get(),
            database = get()
        )
    } bind (Repository::class)
}